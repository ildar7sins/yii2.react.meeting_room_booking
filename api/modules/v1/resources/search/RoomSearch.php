<?php

namespace api\modules\v1\resources\search;

use yii\helpers\BaseInflector;
use api\modules\v1\resources\Room;

class RoomSearch extends Room {
	/**
	 * @var mixed|null
	 */
	public $limit = 12;
	
	/**
	 * @var mixed|null
	 */
	public $page = 1;
	
	/**
	 * @var int|null
	 */
	public $offset;
	
	/**
	 * @var mixed|null
	 */
	public $order_by;
	
	/**
	 * @var mixed|null
	 */
	public $ascending;
	
	/**
	 * @var array|null
	 */
	public $additions;
	/**
	 * @var int|null
	 */
	public $min_price;
	/**
	 * @var int|null
	 */
	public $max_price;
	
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['min_price', 'max_price', 'people', 'date', 'page'], 'integer'],
			[['limit', 'page', 'title', 'description', 'plan', 'order_by', 'ascending', 'offset', 'additions'], 'safe'],
		];
	}
	
	/**
	 * @param $params
	 */
	public function search($params) {
		$this->load($params, '');
		$query = Room::find();
		$query->where(['status' => Room::STATUS_ACTIVE]);
		
		$query->andFilterWhere(['<=', 'people', $this->people]);
		
		if ($this->min_price !== null && $this->max_price !== null) {
			$query->andFilterWhere(['BETWEEN', 'price', $this->min_price, $this->max_price]);
		} else if ($this->max_price !== null) {
			$query->andFilterWhere(['<=', 'price', $this->max_price]);
		} else if ($this->min_price !== null) {
			$query->andFilterWhere(['>=', 'price', $this->min_price]);
		}
		
		
		if ($this->order_by) {
			$orderBy = BaseInflector::underscore($this->order_by);
			$orderBy = self::hasAttribute($orderBy) ? $orderBy : (self::hasAttribute("{$orderBy}_id") ? "{$orderBy}_id" : null);
			
			if ($orderBy) {
				$query->orderBy("{$orderBy} " . ($this->ascending == 1 ? 'ASC' : 'DESC'));
			}
		} else {
			$query->orderBy('updated_at DESC');
		}
		
		$query->offset(--$this->page * $this->limit);
		
		return [
			'count' => $query->count(),
			'data' => $query->limit($this->limit)->all()
		];
	}
}
