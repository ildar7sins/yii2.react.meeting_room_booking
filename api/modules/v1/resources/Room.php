<?php

namespace api\modules\v1\resources;

use common\traits\ClientResource;
use common\models\RoomAttachment;
use yii\web\BadRequestHttpException;

class Room extends \common\models\Room {
	use ClientResource;
	
	const FIELDS_SIMPLE = 'SIMPLE';
	
	public function fields(): array {
		if ($this->modelType === self::FIELDS_SIMPLE) {
			return [
				'id',
				'title',
				'description',
				'people',
				'price',
				'photos',
				'additionalServices'
			];
		}
		
		return [
			'id',
			'title',
			'description',
			'people',
			'price',
			'plan' => static fn($model) => json_decode($model->plan),
			'photos',
			'additionalServices'
		];
	}
	
	public function getPhotos(): array {
		return RoomAttachment::find()
			->select('CONCAT_WS("/", `base_url`, `path`)')
			->where(['room_id' => $this->id])
			->orderBy('order')
			->column();
	}
	
	public function getOrdered(): array {
		if (!isset($_GET['date'])) {
			throw new BadRequestHttpException('No date');
		}
		
		$date = $_GET['date'];
		$dateStart = getUnixFromStr($date, '00:01');
		$dateEnd = getUnixFromStr($date, '23:59');
		
		return [];
	}
	
	/**
	 * @param string $date
	 * @param string $time
	 * @return int
	 */
	public static function getUnixFromStr(string $date, string $time) {
		$datetime = \DateTime::createFromFormat("d.m.y H:i", $date . " {$time}", new \DateTimeZone('UTC'));
		return $datetime->getTimestamp();
	}
}
