<?php

namespace api\modules\v1\controllers;

use common\actions\ViewAction;
use api\modules\v1\resources\Room;
use api\modules\v1\models\Controller;
use api\modules\v1\resources\search\RoomSearch;

/**
 * Room controller
 *
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     basePath="/api/v1/",
 *     @SWG\SecurityScheme(
 *        securityDefinition="Bearer",
 *        type="apiKey",
 *        name="Authorization",
 *        in="header",
 *     ),
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Yii2-Starter-Kit API Documentation",
 *         description="API description...",
 *         termsOfService="",
 *         @SWG\License(
 *             name="BSD License",
 *             url="https://raw.githubusercontent.com/yii2-starter-kit/yii2-starter-kit/master/LICENSE.md"
 *         )
 *     ),
 * )
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class RoomController extends Controller {
	
	public $modelClass = Room::class;
	
	/**
	 * {@inheritdoc}
	 *
	 * @SWG\Get(path="/room/view",
	 *     tags={"Room"},
	 *     security={{"Bearer": {}}},
	 *     summary="Retrieves the collection of rooms.",
	 *     @SWG\Response(
	 *         response = 200,
	 *         description = "One room with full info",
	 *     ),
	 *     @SWG\Parameter(in="query", name="id", type="integer", description="Room ID"),
	 *     @SWG\Parameter(in="query", name="id", type="integer", description="Room ID"),
	 * )
	 */
	public function actions(): array {
		return [
			'view' => [
				'class' => ViewAction::class,
				'modelClass' => $this->modelClass,
				'singleLabel' => 'Заявка'
			],
		];
	}
	
	/**
	 * @SWG\Get(path="/room/index",
	 *     tags={"Room"},
	 *     security={{"Bearer": {}}},
	 *     summary="Retrieves the collection of rooms.",
	 *     @SWG\Response(
	 *         response = 200,
	 *         description = "Article collection response",
	 *     ),
	 *     @SWG\Parameter(in="query", name="minPrice", type="integer", description="minPrice"),
	 *     @SWG\Parameter(in="query", name="maxPrice", type="integer", description="maxPrice"),
	 *     @SWG\Parameter(in="query", name="people", type="integer", description="people"),
	 *     @SWG\Parameter(in="query", name="date", type="integer", description="date"),
	 *     @SWG\Parameter(in="query", name="additions", type="integer", description="additions"),
	 *     @SWG\Parameter(in="query", name="ascending", type="integer", description="ascending"),
	 *     @SWG\Parameter(in="query", name="page", type="integer", description="page"),
	 * )
	 **/
	public function actionIndex() {
		Room::setSerializeType(Room::FIELDS_SIMPLE);
		$roomSearch = new RoomSearch();
		
		return $roomSearch->search($_GET);
	}
}
