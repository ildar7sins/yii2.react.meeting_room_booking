<?php

namespace api\modules\v1\models;

use Yii;
use yii\filters\auth\HttpBearerAuth;
use backend\models\search\FileStorageItemSearch;

class Controller extends \yii\rest\Controller {
	/**
	 * @var string the model class name. This property must be set.
	 */
	public $modelClass;
	
	const CORS = [
		'Origin' => ['*'],
		'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
		'Access-Control-Allow-Credentials' => false,
		'Access-Control-Request-Headers' => ['*'],
		'Access-Control-Allow-Origin:' => ['*'],
	];
	
	public $requestFrom = 'APP';
	
	public const TYPE_APP = 'APP';
	public const TYPE_SITE = 'SITE';
	
	/**
	 * @return array|array[]
	 */
	public function behaviors(): array {
		\Yii::$app->language = 'ru-RU';
		$behaviors = parent::behaviors();
		
		// add CORS filter
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => \api\modules\v1\models\Controller::CORS
		];
		
		unset($behaviors['authenticator']);
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className()
		];
		
		return $behaviors;
	}
	
	public function runAction($route, $params = []) {
		$headers = Yii::$app->request->headers;
		
		if (isset($headers['Request-From']) && $headers['Request-From']) {
			$this->requestFrom = $headers['Request-From'];
		}
		
		return parent::runAction($route, $params);
	}
	
	/**
	 * @return array
	 */
	public function actions(): array {
		return [];
	}
	
	/**
	 * {@inheritdoc}
	 */
	protected function verbs() {
		return [
			'index' => ['GET', 'HEAD', 'OPTIONS'],
			'view' => ['GET', 'HEAD', 'OPTIONS'],
			'create' => ['POST', 'OPTIONS'],
			'update' => ['PUT', 'PATCH', 'OPTIONS'],
			'delete' => ['DELETE', 'OPTIONS'],
		];
	}
	
}
