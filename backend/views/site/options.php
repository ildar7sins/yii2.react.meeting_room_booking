<?php

/**
 * @var yii\web\View $this
 * @var common\models\Переговорную $model
 */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;

\common\extensions\AssetsRegister::registerViewAssets([\backend\assets\BackendAsset::class],
	'@frontendUrl', $this,
	['/backend/js/options.js'],
	[]
);

$this->registerJsVar('maxPeople', Yii::$app->keyStorage->get('maxPeople', 100));
$this->registerJsVar('dateStart', Yii::$app->keyStorage->get('dateStart', 8));
$this->registerJsVar('dateEnd', Yii::$app->keyStorage->get('dateEnd', 20));
$this->registerJsVar('timeOptions', \backend\models\System::getDayHours());
?>
<div class="col-12" id="options">
    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="form-group mr-3">
                <label for="addition-title">Макс. заполненость (%)</label>
                <input type="number" class="form-control" @blur="changed('maxPeople')" v-model="maxPeople" max="100"
                       aria-invalid="false">
            </div>
            <div class="form-group mr-3">
                <label for="addition-title">Время работы (от - до)</label>
                <div class="d-flex flex-row flex-wrap">
                    <select class="form-control transparent-select w-50" v-model="dateStart" @change="changed
                    ('dateStart')">
                        <option v-for="time in timeOptions" :value="time.value" :key="time.value">
                            {{time.label}}
                        </option>
                    </select>
                    <select class="form-control transparent-select w-50" v-model="dateEnd" @change="changed
                    ('dateEnd')">
                        <option v-for="time in timeOptions" :value="time.value" :key="time.value">
                            {{time.label}}
                        </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

