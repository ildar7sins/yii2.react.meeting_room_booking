<?php

/**
 * @var yii\web\View $this
 * @var common\models\AdditionalService $model
 */

$this->title = 'Создать доп. услугу';
$this->params['breadcrumbs'][] = ['label' => 'Дополнительные услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-service-create">
	
	<?php echo $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
