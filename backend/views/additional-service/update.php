<?php

/**
 * @var yii\web\View $this
 * @var common\models\AdditionalService $model
 */

$this->title = 'Отредактировать Additional Service: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Дополнительные услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Отредактировать';
?>
<div class="additional-service-update">
	
	<?php echo $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
