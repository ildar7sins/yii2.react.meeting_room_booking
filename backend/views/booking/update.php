<?php

/**
 * @var yii\web\View $this
 * @var common\models\Booking $model
 */

$this->title = 'Update Booking: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'room_id' => $model->room_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="booking-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
