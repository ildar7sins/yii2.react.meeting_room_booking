<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\models\Booking;
use common\grid\EnumColumn;

/**
 * @var yii\web\View $this
 * @var common\models\search\BookingSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Бронирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">
    <div class="card">
        <div class="card-header">
            <button class='btn btn-success disabled' disabled>Создать</button>
        </div>

        <div class="card-body p-0">
			<?= GridView::widget([
				'layout' => "{items}\n{pager}",
				'options' => [
					'class' => ['gridview', 'table-responsive'],
				],
				'tableOptions' => [
					'class' => ['table', 'text-nowrap', 'table-striped', 'table-bordered', 'mb-0'],
				],
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					
					'id',
					[
						'attribute' => 'room_id',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							$room = \common\models\Room::find()->where(['id' => $model->room_id])->asArray()->one();
							return Html::a($room['title'], "room/view?id={$room['id']}");
						}
					],
					[
						'attribute' => 'date_start',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							return Yii::$app->formatter->asDateTime($model->created_at);
						}
					],
					[
						'attribute' => 'date_end',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							return ($model->date_end - $model->date_start) / 60;
						}
					],
					[
						'attribute' => 'customer_id',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							$user = User::findOne($model->customer_id);
							return Html::a($user->userProfile->getFullName(), "user/view?id={$user->id}");
						}
					],
					'price',
					[
						'class' => EnumColumn::class,
						'attribute' => 'status',
						'enum' => Booking::statuses(),
						'filter' => Booking::statuses()
					],
					
					[
						'class' => \common\widgets\ActionColumn::class,
						'template' => '{view}',
					],
				],
			]); ?>

        </div>
        <div class="card-footer">
			<?php echo getDataProviderSummary($dataProvider) ?>
        </div>
    </div>

</div>
