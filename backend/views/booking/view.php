<?php

use yii\helpers\Html;
use common\models\User;
use common\models\Booking;
use yii\widgets\DetailView;
use common\models\BookingAddition;

/**
 * @var yii\web\View $this
 * @var common\models\Booking $model
 */

$user = User::findOne($model->customer_id);


$this->title = 'Новое бронирование от: ' . $user->userProfile->getFullName();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-view">
    <div class="card">
        <div class="card-header">
			<?php echo Html::a('Подтвердить', ['confirm', 'id' => $model->id, 'room_id' => $model->room_id], ['class' =>
				'btn btn-primary']) ?>
			<?php echo Html::a('Отмена', ['cancel', 'id' => $model->id, 'room_id' => $model->room_id], ['class' => 'btn btn-danger']) ?>
        </div>
        <div class="card-body">
			<?php echo DetailView::widget([
				'model' => $model,
				'attributes' => [
					'id',
					[
						'attribute' => 'room_id',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							$room = \common\models\Room::find()->where(['id' => $model->room_id])->asArray()->one();
							return Html::a($room['title'], "room/view?id={$room['id']}");
						}
					],
					[
						'attribute' => 'date_start',
						'value' => function ($model) {
							return Yii::$app->formatter->asDateTime($model->date_start);
						}
					],
					[
						'attribute' => 'date_end',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) {
							return ($model->date_end - $model->date_start) / 60;
						}
					],
					[
						'attribute' => 'created_at',
						'value' => function ($model) {
							return Yii::$app->formatter->asDateTime($model->created_at);
						}
					],
					[
						'attribute' => 'customer_id',
						'format' => 'raw',
						'filter' => false,
						'value' => function ($model) use ($user) {
							return Html::a($user->userProfile->getFullName(), "user/view?id={$user->id}");
						}
					],
					[
						'attribute' => 'status',
						'value' => function ($model) {
							return Booking::statuses()[$model->status];
						}
					],
					'comment',
					[
						'attribute' => 'id',
						'format' => 'raw',
						'label' => 'Дополненительно',
						'value' => function ($model) {
							$additions = BookingAddition::find()
								->joinWith('addition')
								->where(['booking_id' => $model->id])
								->asArray()
								->all();
							
							return implode(', ', array_map(static function ($item) {
								return "<span>{$item['addition']['title']} - {$item['count']} шт</span>";
							}, $additions));
						}
					],
					'price',
				],
			]) ?>
        </div>
    </div>
</div>
