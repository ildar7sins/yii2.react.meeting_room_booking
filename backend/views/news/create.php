<?php

/**
 * @var yii\web\View $this
 * @var backend\models\News $model
 */

$this->title = 'Create News';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
