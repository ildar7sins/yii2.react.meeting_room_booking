<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \backend\models\LoginForm */

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';

Yii::$app->language = 'ru-RU';
?>
<div class="login-box" id="login-page">
    <div class="login-logo">
        <img src="/img/logo.svg" alt="logo">
    </div>

    <div class="card">
        <div class="card-body login-card-body">

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?php echo $form->errorSummary($model) ?>
            <?php echo $form->field($model, 'username', [
                'inputTemplate' => '<div class="input-group">
                    {input}
                    <div class="input-group-append"><span class="input-group-text"><span class="fas fa-user"></span></span></div>
                </div>',
            ])->label('Email') ?>
            <?php echo $form->field($model, 'password', [
                'inputTemplate' => '<div class="input-group">
                    {input}
                    <div class="input-group-append"><span class="input-group-text"><span class="fas fa-lock"></span></span></div>
                </div>',
            ])->passwordInput() ?>
            <?php echo $form->field($model, 'rememberMe')->checkbox() ?>

            <?php echo Html::submitButton(Yii::t('backend', 'Sign In'). ' <span class="fas fa-arrow-right fa-sm"></span>', [
                'class' => 'btn btn-primary btn-block',
                'name' => 'login-button'
            ]) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
