<?php

/**
 * @var yii\web\View $this
 * @var common\models\Room $model
 */

$this->title = 'Update Переговорную: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Переговорнуюs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="room-update">
	
	<?php echo $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
