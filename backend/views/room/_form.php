<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use trntv\filekit\widget\Upload;
use common\models\AdditionalService;
use common\extensions\AssetsRegister;


/**
 * @var yii\web\View $this
 * @var common\models\Room $model
 * @var yii\bootstrap4\ActiveForm $form
 */


AssetsRegister::registerViewAssets([\backend\assets\BackendAsset::class],
	'@frontendUrl', $this,
	["/js/konva.min.js", "/js/vue-konva.min.js", '/backend/js/room/form.js'],
	['/backend/css/room/form.css']
);

$this->registerJsVar('roomPlan', $model->plan ?: "[]");
?>
<div class="room-form" id="room-form">
	<?php $form = ActiveForm::begin(['options' => ['@submit' => 'checkForm']],); ?>
    <div class="card">
        <div class="card-body">
			<?= $form->errorSummary($model); ?>
			
			<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'people')->textInput() ?>
			<?= $form->field($model, 'price')->textInput() ?>
			<?= $form->field($model, 'attachments')->widget(
				Upload::class,
				[
					'url' => ['/file/storage/upload'],
					'sortable' => true,
					'maxFileSize' => 10000000, // 10 MiB
					'maxNumberOfFiles' => 10,
					'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
				]
			) ?>
			
			<?= $form->field($model, 'services')->widget(Select2::classname(), [
				'data' => ArrayHelper::map(
					AdditionalService::find()
						->select('id, title')
						->where(['status' => 1])
						->all(),
					'id', 'title'),
				'language' => 'ru',
				'options' => [
					'placeholder' => 'Выбрать...',
					'multiple' => true,
				],
			]) ?>
			
			
			<?= $form->field($model, 'status')->checkbox() ?>
			
			<?= $form->field($model, 'plan')->hiddenInput(['id' => 'plan-input']) ?>

            <div class="plan-container">
                <div class="plan-wrapper">
                    <v-stage ref="stageArea" :config="stageConfig" @mousedown="handleStageMouseDown">
                        <v-layer>
                            <v-group ref="imageGroup">
                                <v-image v-for="image in images" :key="image.id" :config="image"/>
                            </v-group>
                            <v-transformer ref="transformer"/>
                        </v-layer>
                    </v-stage>
                </div>
                <div class="plan-buttons px-3">
                    <span @click="addItem(item)" v-for="item in furniture" class="btn btn-success btn-sm mb-2">Добавить {{item.label}}</span>
                    <span @click="removeSelectedImage" class="btn btn-danger btn-sm">Удалить выбранный объект</span>
                </div>
            </div>
        </div>
        <div class="card-footer">
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>
