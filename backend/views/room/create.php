<?php

/**
 * @var yii\web\View $this
 * @var common\models\Переговорную $model
 */

$this->title = 'Создать Переговорную';
$this->params['breadcrumbs'][] = ['label' => 'Переговорнуюs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-create">
	
	<?php echo $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
