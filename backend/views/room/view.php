<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Переговорную $model
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Переговорнуюs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-view">
    <div class="card">
        <div class="card-header">
			<?php echo Html::a('Отредактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			<?php echo Html::a('Удалить', ['delete', 'id' => $model->id], [
				'class' => 'btn btn-danger',
				'data' => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method' => 'post',
				],
			]) ?>
        </div>
        <div class="card-body">
			<?php echo DetailView::widget([
				'model' => $model,
				'attributes' => [
					'id',
					'title',
					'description',
					'people',
					'price',
					'created_by',
					'created_at',
					'updated_by',
					'updated_at',
					'status',
					'plan',
				
				],
			]) ?>
        </div>
    </div>
</div>
