<?php

/**
 * @var yii\web\View $this
 * @var backend\models\City $model
 */

$this->title = 'Create City';
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
