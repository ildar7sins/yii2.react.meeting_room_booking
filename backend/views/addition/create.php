<?php

/**
 * @var yii\web\View $this
 * @var common\models\Addition $model
 */

$this->title = 'Create Addition';
$this->params['breadcrumbs'][] = ['label' => 'Additions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addition-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
