<?php

/**
 * @var yii\web\View $this
 * @var common\models\Addition $model
 */

$this->title = 'Update Addition: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Additions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="addition-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
