let DashboardPage = new Vue({
	el: "#options",
	data() {
		return {
			maxPeople,
			dateStart,
			dateEnd,
			timeOptions,
		}
	},
	mounted() {
	},
	methods: {
		async changed(type) {
			const result = await apiPost(`/backend/system/key-storage/update?key=${type}`, {
				key: type,
				value: this[type],
			});
			
			if (result) {
				console.log(result)
			}
		}
	}
});
