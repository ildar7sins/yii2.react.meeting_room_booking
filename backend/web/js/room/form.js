const roomPlanArray = JSON.parse(roomPlan);
console.log(roomPlanArray)

let DashboardPage = new Vue({
	el: "#room-form",
	data() {
		return {
			furniture: [
				{
					label: 'Стол',
					url: '/img/table.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Стул',
					url: '/img/chair.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Тумба',
					url: '/img/tumba.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Диван',
					url: '/img/sofa.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Стена',
					url: '/img/wall.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Кресло',
					url: '/img/chair2.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Телевизор',
					url: '/img/tv.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
				{
					label: 'Дверь',
					url: '/img/door.png',
					x: 0,
					y: 0,
					height: '100px',
					width: '100px',
					scale: 0.2
				},
			],
			stageConfig: {
				width: 660, height: 350
			},
			urls: roomPlanArray,
			images: {},
			selectedNode: null,
			imageGroup: null,
			transformer: null,
			stageArea: null,
			dragItemId: null,
		}
	},
	mounted() {
		this.transformer = this.$refs.transformer.getStage();
		this.imageGroup = this.$refs.imageGroup.getStage();
		this.stageArea = this.$refs.stageArea.getStage();
		
		this.registerImages();
	},
	methods: {
		handleStageMouseDown(e) {
			if (e.target === e.target.getStage()) {
				this.selectedImageName = "";
				this.selectedNode = null;
				this.updateTransformer();
				return;
			}
			// clicked on transformer - do nothing
			const clickedOnTransformer =
				e.target.getParent().className === "Transformer";
			if (clickedOnTransformer) {
				return;
			}
			// find clicked rect by its name
			const name = e.target.name();
			const rect = Object.values(this.images).find(r => r.name === name);
			if (rect) {
				this.selectedImageName = name;
				this.selectedNode = this.imageGroup.find(
					".".concat(this.selectedImageName)
				)[0];
				
				this.selectedNode.moveToTop();
			}
			this.updateTransformer();
		},
		updateTransformer() {
			// here we need to manually attach or detach Transformer node
			const transformerNode = this.transformer;
			if (this.cropMode) {
				transformerNode.detach();
			} else if (this.selectedNode === transformerNode.node()) {
				// do nothing if selected node is already attached
				return;
			} else if (this.selectedNode) {
				// attach to another node
				this.selectedNode.moveToTop();
				transformerNode.attachTo(this.selectedNode);
			} else {
				// remove transformer
				transformerNode.detach();
			}
			transformerNode.getLayer().batchDraw();
		},
		registerImages() {
			this.urls.forEach(item => {
				console.log(item)
				let img = new Image();
				img.src = item.name.replace(/[0-9]/g, '');
				;
				img.onload = () => {
					// {"id":"/img/tv.png9","draggable":true,"name":"/img/tv.png9","scaleX":0.19999999999999998,"scaleY":0.2,"rotation":90.20719368881728}
					this.$set(this.images, item.name, {
						image: img,
						...item
					});
				};
			});
		},
		addItem(item) {
			const random = Math.floor(Math.random() * Math.floor(100))
			let img = new Image();
			img.src = item.url;
			img.onload = () => {
				this.$set(this.images, item.url + random, {
					id: item.url + random,
					image: img,
					draggable: true,
					name: item.url + random,
					x: item.x,
					y: item.y,
					scaleX: item.scale,
					scaleY: item.scale,
				});
			};
		},
		removeSelectedImage() {
			this.urls = this.urls.filter(
				url => url !== this.selectedImageName
			);
			
			this.$delete(this.images, this.selectedImageName);
			this.selectedNode = null;
			this.updateTransformer();
			
			// this.stageArea.draw();
		},
		load() {
			const data = localStorage.getItem('storage') || '[]';
			this.list = JSON.parse(data);
		},
		
		save() {
			const transformerNode = this.transformer;
			transformerNode.detach();
			
			let result = [];
			
			const json = this.$refs.stageArea.getStage().toJSON();
			const object = JSON.parse(json);
			
			const res = object.children[0].children[0].children;
			
			if (res.length > -1) {
				for (let item in res) {
					result.push({
						...res[item].attrs,
						x: res[item].attrs.x || 0,
						y: res[item].attrs.y || 0,
					});
				}
			}
			
			document.getElementById('plan-input').value = JSON.stringify(result);
		},
		checkForm: function (e) {
			this.save();
			
			return true;
		}
	}
});
