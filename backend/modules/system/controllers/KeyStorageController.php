<?php

namespace backend\modules\system\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\KeyStorageItem;
use yii\web\NotFoundHttpException;
use common\traits\FormAjaxValidationTrait;
use backend\modules\system\models\search\KeyStorageItemSearch;

/**
 * KeyStorageController implements the CRUD actions for KeyStorageItem model.
 */
class KeyStorageController extends Controller {
	use FormAjaxValidationTrait;
	
	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action); // TODO: Change the autogenerated stub
	}
	
	/** @inheritdoc */
	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
	
	/**
	 * Lists all KeyStorageItem models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$model = new KeyStorageItem();
		
		$this->performAjaxValidation($model);
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return 'ok';
		} else {
			$searchModel = new KeyStorageItemSearch();
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			$dataProvider->sort = [
				'defaultOrder' => ['key' => SORT_DESC],
			];
			
			return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'model' => $model,
			]);
		}
	}
	
	/**
	 * Updates an existing KeyStorageItem model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($key) {
		$data = Yii::$app->query->getData();
		$model = KeyStorageItem::find()->where(['key' => $key])->one() ?: new KeyStorageItem();
		$model->key = $data['key'];
		$model->value = $data['value'];
		
		$this->performAjaxValidation($model);
		
		if ($model->save()) {
			return 'ok';
		} else {
			var_dump($model->getErrors());
		}
	}
	
	/**
	 * Deletes an existing KeyStorageItem model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		
		return $this->redirect(['index']);
	}
	
	/**
	 * Finds the KeyStorageItem model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return KeyStorageItem the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = KeyStorageItem::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
