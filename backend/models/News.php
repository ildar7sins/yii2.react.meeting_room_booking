<?php

namespace backend\models;

use backend\models\search\CitySearch;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $body
 * @property string|null $thumbnail_base_url
 * @property string|null $thumbnail_path
 * @property int $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property City[] $cities
 * @property CityNews[] $cityNews
 * @property User $createdBy
 * @property FavoriteNews[] $favoriteNews
 * @property News[] $news
 * @property SimilarNews[] $similarNews
 * @property SimilarNews[] $similarNews0
 * @property News[] $similarNews1
 * @property User $updatedBy
 * @property User[] $users
 */
class News extends \yii\db\ActiveRecord {

	const STATUS_NEW = 0;
	const STATUS_PUBLISHED = 1;

	public $thumbnail;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'description', 'body'], 'required'],
            [['body'], 'string'],
            [['status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 512],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
			[['city', 'similar', 'thumbnail'], 'safe'],
		];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'body' => 'Текст',
            'thumbnail_base_url' => '',
            'thumbnail_path' => '',
            'status' => 'Статус',
            'city' => 'Город',
            'similar' => 'Похожие объявления',
            'thumbnail' => 'Миниатюра',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Создана',
            'updated_at' => 'Updated At',
        ];
    }


	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::class,
			BlameableBehavior::class,
			[
				'class' => UploadBehavior::class,
				'attribute' => 'thumbnail',
				'pathAttribute' => 'thumbnail_path',
				'baseUrlAttribute' => 'thumbnail_base_url',
			],
		];
	}

	/**
	 * @return array statuses list
	 */
	public static function statuses() {
		return [
			self::STATUS_NEW => 'Черновик',
			self::STATUS_PUBLISHED => 'Опубликована',
		];
	}

	public function getCity() {
		return City::find()
			->joinWith('news')
			->where(['`city_news`.`news_id`' => $this->id])
			->one();
	}

	public function setCity($value) {
		CityNews::deleteAll(['news_id' => $this->id]);

		if($value) {
			$city = new CityNews();
			$city->city_id = (int)$value;
			$city->news_id = $this->id;
			$city->save();
		}
	}

	public function getSimilar() {
		return self::find()
			->select('id, title')
			->joinWith('similarNews', false)
			->where(['`similar_news`.`news_id`' => $this->id])
			->all();
	}

	public function setSimilar($value) {
		SimilarNews::deleteAll(['news_id' => $this->id]);
		if($value) {
			foreach ($value as $similar) {
				$city = new SimilarNews();
				$city->news_id = $this->id;
				$city->similar_news_id = (int)$similar;
				$city->save();
			}

//			var_dump($value);
//			die;
		}
	}

    /**
     * Gets query for [[Cities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['id' => 'city_id'])->viaTable('city_news', ['news_id' => 'id']);
    }

    /**
     * Gets query for [[CityNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCityNews()
    {
        return $this->hasMany(CityNews::className(), ['news_id' => 'id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[FavoriteNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteNews()
    {
        return $this->hasMany(FavoriteNews::className(), ['news_id' => 'id']);
    }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['id' => 'news_id'])->viaTable('similar_news', ['similar_news_id' => 'id']);
    }

    /**
     * Gets query for [[SimilarNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarNews0()
    {
        return $this->hasMany(SimilarNews::className(), ['news_id' => 'id']);
    }

    /**
     * Gets query for [[SimilarNews0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarNews()
    {
        return $this->hasMany(SimilarNews::className(), ['similar_news_id' => 'id']);
    }

    /**
     * Gets query for [[SimilarNews1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarNews1()
    {
        return $this->hasMany(News::className(), ['id' => 'similar_news_id'])->viaTable('similar_news', ['news_id' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('favorite_news', ['news_id' => 'id']);
    }
}
