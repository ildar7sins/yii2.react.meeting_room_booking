<?php
return [
	'class' => yii\web\UrlManager::class,
	'enablePrettyUrl' => true,
	'showScriptName' => false,
	'rules' => [
		['pattern' => 'options', 'route' => 'site/options'],
	]
];
