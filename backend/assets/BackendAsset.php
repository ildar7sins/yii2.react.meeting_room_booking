<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\YiiAsset;
use yii\web\AssetBundle;
use common\assets\AdminLte;
use common\assets\Html5shiv;

class BackendAsset extends AssetBundle {
	/**
	 * @var string
	 */
	public $sourcePath = '@backend/web';
	
	/**
	 * @var array
	 */
	public $css = [
		// '/css/style.css',
		'css/login.css',
	];
	/**
	 * @var array
	 */
	public $js = [
		'/js/axios.min.js',
		'/js/api.js',
		YII_ENV_DEV ? '/js/vue.js' : '/js/vue.min.js',
	];
	
	public $publishOptions = [
		'only' => [
			'*.css',
			'*.js',
			'../img/*'
		],
		"forceCopy" => YII_ENV_DEV,
	];
	
	/**
	 * @var array
	 */
	public $depends = [
		YiiAsset::class,
		AdminLte::class,
		Html5shiv::class,
	];
}
