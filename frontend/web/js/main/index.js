const { useEffect, useState } = React;

function App(props) {
  const [rooms, setRooms] = useState([]);

  useEffect(async () => {
    const res = await apiGet(`${API_URL}/room`);
    console.log(res);
  });

  return (
    <div className="container">
      <div className="card">
        <div className="card-body">
          <h3 className="card-title">
            Найдите подходящую комнату и забронируйте
          </h3>
          <div>
            <form>
              <div className="row">
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Выберите даты:</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Количество мест:</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Площадь:</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Стоимость:</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
              <div className="row">
                <label>Наличие в комнате: </label>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
                  <label class="form-check-label" for="inlineCheckbox1">Wi-Fi</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
                  <label class="form-check-label" for="inlineCheckbox2">ВКС</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" />
                  <label class="form-check-label" for="inlineCheckbox2">LED</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option4" />
                  <label class="form-check-label" for="inlineCheckbox2">Микрофон</label>
                </div>
              </div>
              <div className="row">
                <a href="#" class="btn btn-primary" role="button" data-bs-toggle="button"> Найти</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

const domContainer = document.querySelector("#app");
ReactDOM.render(<App />, domContainer);
