function errrorHandler(e) {
	console.error(e);
	
	return false;
}

function getHeaders() {
	if (user_access_token) {
		return { Authorization: `Bearer ${user_access_token}` };
	}
	
	return {};
}

const apiPost = async (url, data = {}) => {
	try {
		const response = await axios.post(url, data, { headers: getHeaders() });
		
		return response.data;
	} catch ( e ) {
		errrorHandler(e);
	}
}

const apiGet = async (url, data) => {
	if (data) {
		let getParams = await Object.keys(data).map(function (key) {
			return key + '=' + data[key];
		}).join('&');
		
		url += `?${getParams}`
	}
	
	const headers = getHeaders();
	
	try {
		const response = await axios.get(url, { headers });
		
		return response.data;
	} catch ( e ) {
		errrorHandler(e);
	}
}

const apiDelete = async (url, data) => {
	if (data) {
		let getParams = await Object.keys(data).map(function (key) {
			return key + '=' + data[key];
		}).join('&');
		
		url += `?${getParams}`
	}
	
	try {
		const response = await axios.delete(url, { headers: getHeaders() });
		
		return response.data;
	} catch ( e ) {
		errrorHandler(e);
	}
}

const apiPatch = async (url, data) => {
	try {
		const response = await axios.patch(url, data, { headers: getHeaders() });
		return response.data;
	} catch ( e ) {
		errrorHandler(e);
	}
}

const apiPut = async (url, data) => {
	try {
		const response = await axios.put(url, data, { headers: getHeaders() });
		return response.data;
	} catch ( e ) {
		errrorHandler(e);
	}
}

