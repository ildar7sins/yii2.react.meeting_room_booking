const e = React.createElement;
const { Stage, Layer, Rect, Transformer, Image } = ReactKonva;

class LikeButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			liked: 0,
			modalIsOpen: false,
			selectedHours: [],
			selectedRoom: null,
		};
	}
	
	async componentDidMount() {
		const res = await apiGet(`${API_URL}/room/view?id=${16}`);
		this.setState({ selectedRoom: res })
	}
	
	openModal() {
		this.setState({ modalIsOpen: true });
	}
	
	afterOpenModal() {
		// references are now sync'd and can be accessed.
		subtitle.style.color = '#f00';
	}
	
	closeModal() {
		this.setState({ modalIsOpen: false });
	}
	
	toogleSelectedHour(value) {
		const { selectedHours } = this.state;
		console.log(selectedHours)
		console.log(value, selectedHours[selectedHours.length - 1] + 1, value > selectedHours[selectedHours.length - 1] + 1);
		console.log(value, selectedHours[0] - 1, value < selectedHours[0] - 1)
		
		if (selectedHours.includes(value)) {
			const index = selectedHours.indexOf(value);
			selectedHours.splice(index, 1);
		} else {
			selectedHours.push(value);
		}
		
		selectedHours.sort(function (a, b) {
			return a - b;
		})
		this.setState({ selectedHours });
	}
	
	renderHourItems() {
		const end = parseInt(dateEnd);
		const start = parseInt(dateStart);
		const length = end - start;
		let current = start;
		
		return Array.from({ length: length }, (v, i) => {
			let res = {
				title: '',
				value: current
			}
			res.title += current > 9 ? (current + ':00 - ') : ('0' + current + ':00 - ');
			current++;
			res.title += current > 9 ? (current + ':00') : ('0' + current + ':00');
			
			return res;
		}).map(item => {
			let classes = "hour-select";
			classes += this.state.selectedHours.includes(item.value) ? ' active' : '';
			return (
				<div className={classes} key={item.value} onClick={() => this.toogleSelectedHour(item.value)}>
					<span>{item.title}</span>
				</div>
			);
		})
	}
	
	render() {
		const { liked, modalIsOpen, selectedRoom } = this.state;
		
		return (
			<div>
				<TestComponent title={'Hello component'}/>
				<button onClick={() => this.openModal()}>Open modal</button>
				<ReactModal
					isOpen={modalIsOpen}
					onRequestClose={() => this.closeModal()}
					contentLabel="Example Modal"
					className="Modal"
					overlayClassName="Overlay"
				>
					
					<div className="row">
						<div className="col-12">
							<div className="row">
								<div className="col-4">Слайдер</div>
								<div className="col-8">
									Описание + план
									<Stage width={660} height={350}>
										<Layer>
											{selectedRoom && selectedRoom.plan.map((rect, i) => {
												return <UrlImage key={i} img={rect}/>
												
											})}
										</Layer>
									</Stage>
								</div>
							</div>
						</div>
						<div className="col-12">
							<div className="row">
								<div className="col-8">Форма заказа</div>
								<div className="col-4">
									<div className="date-select d-flex flex-column">
										<div className="date">
											<h1>Data</h1>
										</div>
										<div className="hours">
											{this.renderHourItems()}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</ReactModal>
			</div>
		);
	}
	
}

const UrlImage = ({ img }) => {
	const [image, status] = useImage(img.name.replace(/[0-9]/g, ''));
	
	return <Image
		id={img.id}
		draggable={false}
		scaleX={img.scaleX}
		scaleY={img.scaleY}
		x={img.x}
		y={img.y}
		rotation={img.rotation}
		name={img.name}
		image={image}
	/>;
	
	
};


const domContainer = document.querySelector('#app');
ReactDOM.render(e(LikeButton), domContainer);
