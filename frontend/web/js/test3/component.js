let { useState } = React;
/*
  TestComponent ему передаются следующий объект
  Object  =  {
	  photos: [
		  [url:image-url1, href:itemId1 ]
		  [url:image-url1, href:itemId2 ]
		  [url:image-url1, href:itemId3 ]  ... up to 5
	  ],
	  title: title
	  description: desc
  }
*/

const TestComponent = ({ title }) => {
	const [click, setClick] = useState(0)
	const [isShown, setShown] = useState(0)
	const [currentAction, setAction] = useState('none')
	
	const elements = ['apple', 'orange', 'pineapple']
	const items = []
	let marker = "/img/slider-image-" + elements[0] + ".png"
	const firstItem =
		<li className="photo-slider-list-item" data-marker={marker}>
			<div className="photo-slider-item">
				<img className="photo-slider-image" src={marker} itemProp="image"></img>
			</div>
		</li>
	
	
	for (const [index, value] of elements.entries()) {
		let marker = "/img/slider-image-" + value + ".png"
		
		items.push(
			<li className="photo-slider-list-item" data-marker={marker} key={index}>
				<div className="photo-slider-item">
					<img className="photo-slider-image" src={marker} itemProp="image"></img>
				</div>
			</li>
		)
	}
	return (
		<div className="styles-root-1 photo-slider-slider"
			 onMouseEnter={() => setShown(1)}
			 onMouseLeave={() => {
				 setShown(0);
				 setAction('none')
			 }}
		>
			<header className="styles-root-2">
				<a href="/item/view?id=1" className="styles-link">
					<div className="photo-slider-root" data-marker="item-photo">
						<div className="photo-slider photo-slider-aspect-ratio-4-3">
							<ul className="photo-slider-list">
								{isShown ? (items) : (firstItem)}
							</ul>
						</div>
					</div>
				</a>
			</header>
			<div className="body-root">
				<div className="body-titleRow">
					<h3 className="body-title">{title}</h3>
					<div className="body-actions">
						<ul className="body-actions-list">
							<li className="body-actions-list-item">
								<button className="body-action-button"
										onClick={() => setAction('one')}
								>Проектор
								</button>
							</li>
							<li className="body-actions-list-item">
								<button className="body-action-button"
										onClick={() => setAction('two')}
								>Проектор
								</button>
							</li>
							<li className="body-actions-list-item">
								<button className="body-action-button"
										onClick={() => setAction('three')}
								>Проектор
								</button>
							</li>
							<li className="body-actions-list-item">
								<button className="body-action-button"
										onClick={() => setAction('four')}
								>Проектор
								</button>
							</li>
							<li className="body-actions-list-item">
								<button className="body-action-button"
										onClick={() => setAction('five')}
								>Проектор
								</button>
							</li>
						</ul>
					</div>
				</div>
				<div className="body-text">
					
					<p>Эта комната расположена в живописно уголке Уфы на берегу озера,
						потрясающий вид которого не раз оставлял неизгладимое впечатление
						на проживающих в данной комнате... бла бла бла...
					</p>
					<p>{currentAction != 'none' && "Нажато действие " + currentAction}</p>
					<p onClick={() => setClick(click + 1)}>Нажато {click} раз</p>
				
				</div>
			</div>
		
		</div>
	);
}
