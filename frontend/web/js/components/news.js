const NewsTemplate = `
    <div class="news-card">
        <img class="news-thumb pull-left" :src="news.image" alt="">
        <div class="news-content">
            <div>
                <h2 class="news-title">
                    <a :href="'/news/view?id=' + news.id">{{news.title}}</a>
                </h2>

                <div class="news-text">
                    {{news.description | descriptionFilter}}
                </div>
            </div>
            <div>
                <span class="news-date"> {{news.createdAt | day}}</span>
            </div>
        </div>
    </div>
`;

const NewsCard = Vue.component("news-card", {
    template: NewsTemplate,
    props: {
        news: Object,
    },
    data: function () {
        return {}
    },
    methods: {},
    filters: {
        day(value) {
            let format = 'dd.mm.yy';
            const date = new Date(value * 1000);
            const dateParts = {
                yyyy: String(date.getFullYear()),
                yy: String(date.getFullYear()).substr(2),
                mm: ('0' + (date.getMonth() + 1)).slice(-2),
                dd: ('0' + date.getDate()).slice(-2),
                hh: date.getHours(),
                i: ('0' + date.getMinutes()).slice(-2),
            };

            Object.keys(dateParts).map(prop => {
                format = format.replace(new RegExp(prop, 'gi'), dateParts[prop]);
            });

        return format;
        },
        descriptionFilter(text) {
            if(text.length > 190) {
                return text.slice(0, 190) + '...';
            } else {
                return text;
            }
        }
    }
});
