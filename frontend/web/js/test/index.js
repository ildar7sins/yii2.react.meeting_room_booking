const e = React.createElement;
class LikeButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = { liked: 0 };

		this.data = [{id:1, photos:[
			"/img/slider-image-apple.png", 
			"/img/slider-image-pineapple.png", 
			"/img/slider-image-orange.png"
		], title: "Wellcome to UFA", description: "Эта комната расположена в живописно уголке Уфы... бла бла бла..."}]
	}
	
	componentDidMount() {
		this.test();
	};
	
	async test() {
		const res = await apiGet(`${API_URL}/room`)
		console.log(res)
		this.data = res.data
		this.forceUpdate()
	}
	
	render() {
		const { liked } = this.state;

		return (
			<div>
				<p>Count of {liked}</p>
				<button onClick={() => this.setState({ liked: liked + 1 })}>
					Like
				</button>
				{this.data.length > 0 && <TestComponent data={this.data[0]}/>}
			</div>
		);
	}
	
}
				


const domContainer = document.querySelector('#app');
ReactDOM.render(e(LikeButton), domContainer);
