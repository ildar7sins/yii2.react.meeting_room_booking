let { useState } = React;
/*
  TestComponent ему передаются следующий объект
  Object  =  {
	  photos: [
		  [url:image-url1, href:itemId1 ]
		  [url:image-url1, href:itemId2 ]
		  [url:image-url1, href:itemId3 ]  ... up to 5
	  ],
	  title: title
	  description: desc
	  additionalServices: [{id, title}]
  }
*/

const TestComponent = ({ data }) => {
	const [click, setClick] = useState(0)
	const [isShown, setShown] = useState(0)
	//const [currentAction, setAction] = useState('none')

	const elements = data.photos //['apple', 'orange', 'pineapple']
	const items = []
	let marker = elements[0]
	const firstItem = 
		<li className="photo-slider-list-item" data-marker={marker}>
			<div className="photo-slider-item">
				<img className="photo-slider-image" src={marker} itemProp="image"></img>
			</div>
		</li>
	
	
	const services = data.additionalServices
	
	for (const [index, value] of elements.entries()) {
		let marker = value

		if (index==5) break // показывать только первые 5 фоток

 		items.push(
			<li className="photo-slider-list-item" data-marker={marker} key={index}>
				<div className="photo-slider-item">
					<img className="photo-slider-image" src={marker} itemProp="image"></img>
				</div>
			</li>
		)
	}

	const serv = []
	if (services)
	for (const [index, value] of services.entries()) {
		let marker = value
		
		serv.push(
			<li className="body-service-list-item" 
				data-marker={marker.title} key={index}
				
			>
				{marker.title}
			</li>
		)
	}
	return (
		<div className="styles-root-1 photo-slider-slider"
			onMouseEnter={() => setShown(1)}
			onMouseLeave={() => setShown(0)}
		>
			<header className="styles-root-2">
				<a href={"/room/view?id="+data.id} className="styles-link">
					<div className="photo-slider-root" data-marker="item-photo">
						<div className="photo-slider photo-slider-aspect-ratio-4-3">
							<ul className="photo-slider-list">
								{ isShown ? (items) : (firstItem) }								
							</ul>
						</div>
					</div>
				</a>
			</header>
			<div className="body-root">
				<div className="body-titleRow">
					<h3 className="body-title">{data.title}</h3>	
					<div className="body-service">
						<ul className="body-service-list">
							{services && (serv)}							
						</ul>
					</div>			
				</div>
				<div className="body-text">
					
					<p>{data.description}</p>
					<p onClick={() => setClick(click + 1)}>Нажато {click} раз</p>
				
				</div>			
			</div>
		
		</div>
	);
	
}

const SliderComponent = ({data}) =>{
	const items = []
	const elements = data.photos
	for (const [index, value] of elements.entries()) {
		
		if (index==5) break // показывать только первые 5 фоток

 		items.push(
			<li className="photo-slider-list-item" data-marker={value} key={index}>
				<div className="photo-slider-item">
					<img className="photo-slider-image" src={value} itemProp="image" />
				</div>
				<div className="photo-slider-show-item">
					<img className="photo-slider-show-image" src={value} itemProp="image"/>
				</div>
			</li>
		)
	}
	return (
		<div className="styles-root-1 photo-slider-slider">
			<div className="photo-slider-redesign-up">
				<h3>Слайдер</h3>
				<div className="photo-slider-root" data-marker="item-photo">
					<div className="photo-slider-redesign photo-slider-aspect-ratio-4-3">
						<ul className="photo-slider-list">
							{items}
						</ul>
					</div>
				</div>			
			</div>
		</div>
	)
}

const RoomComponent = ({data}) => {
	const plan = <div>Plan</div>
	return (
		<div className="row">
		<span>Описание + план</span>
			<div className="col-12">
				{data.description}
			</div>
			<div className="col-12">
				{plan}
			</div>

		</div>
	)
}