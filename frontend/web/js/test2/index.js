const e = React.createElement;

class LikeButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			liked: 0,
			modalIsOpen: false,
		};

		this.data = [{id:1, photos:[
			"/img/slider-image-apple.png", 
			"/img/slider-image-pineapple.png", 
			"/img/slider-image-orange.png"
		], title: "Wellcome to UFA", description: "Эта комната расположена в живописно уголке Уфы... бла бла бла..."}]
	}
	
	componentDidMount() {
		this.test();
	};
	
	async test() {
		const res = await apiGet(`${API_URL}/room`)
		console.log(res)
		this.data = res.data
		this.forceUpdate()
	}
	
	openModal() {
		this.setState({ modalIsOpen: true });
	}
	
	afterOpenModal() {
		// references are now sync'd and can be accessed.
		subtitle.style.color = '#f00';
	}
	
	closeModal() {
		this.setState({ modalIsOpen: false });
	}
	
	render() {
		const { liked, modalIsOpen } = this.state;
		
		return (
			<div>
				{this.data.length > 0 && <TestComponent data={this.data[0]}/>}
				<button onClick={() => this.openModal()}>Open modal</button>
				<ReactModal
					isOpen={modalIsOpen}
					onRequestClose={() => this.closeModal()}
					contentLabel="Example Modal"
					className="Modal"
					overlayClassName="Overlay"
				>					
				<div className="row">
					<div className="col-12">
						<div className="row">
							<div className="col-4">
							{this.data.length > 0 && 
								<SliderComponent data={this.data[0]}/>
							}								
							</div>
							<div className="col-8">
							{this.data.length > 0 && 
								<RoomComponent data={this.data[0]}/>
							}
							</div>
						</div>
					</div>
					<div className="col-12">
						<div className="row">
							<div className="col-8">Форма заказа</div>
							<div className="col-4">Выбор даты</div>
						</div>
					</div>
				</div>
				</ReactModal>
			</div>
		);
	}
	
}

const domContainer = document.querySelector('#app');
ReactDOM.render(e(LikeButton), domContainer);
