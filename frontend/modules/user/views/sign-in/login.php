<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use frontend\assets\FrontendAsset;
use common\extensions\AssetsRegister;


/**
 * @var yii\web\View $this
 * @var yii\bootstrap4\ActiveForm $form
 * @var frontend\modules\user\models\LoginForm $model
 */

$this->title = Yii::t('frontend', 'Login');

AssetsRegister::registerViewAssets([FrontendAsset::class],
	'@frontendUrl', $this,
	[],
	['/css/login.css']
);
?>

<div class="page-content pt-5">
	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div class="site-login pt-5">
        <div class="row justify-content-center">
            <div class="card">
                <div class="card-body login-card">
                    <div class="login-card__img">
                        <h3>Welcome to</h3>
                        <h3>IQpark</h3>
                    </div>
                    <div class="login-card__form">
                        <h1 class="text-muted text-center"><?= Html::encode($this->title) ?></h1>
						<?= $form->errorSummary($model) ?>
						<?= $form->field($model, 'identity')->label('Email') ?>
						<?= $form->field($model, 'password')->passwordInput() ?>

                        <div class="form-group">
							<?= Html::submitButton(Yii::t('frontend', 'Login'), ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
                        </div>
                        <div class="col-12 social-wrapper">
                            <div class="auth facebook"><i class="fab fa-facebook-f"></i></div>
                            <div class="auth vk"><i class="fab fa-vk"></i></div>
                            <div class="auth google"><i class="fab fa-google"></i></div>
                        </div>
                        <div class="form-group text-center mt-2">
							<?= Html::a('Регистрация', ['signup']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>
