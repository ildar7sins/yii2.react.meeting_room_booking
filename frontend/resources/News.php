<?php

namespace frontend\resources;

use backend\models\FavoriteNews;
use common\traits\ClientResource;

class News extends \backend\models\News {
	use ClientResource;

	const FIELDS_SIMPLE = 'SIMPLE';

	public function fields() {
		if ($this->modelType === self::FIELDS_SIMPLE) {
			return [
				'id',
				'title',
				'description',
				'image',
				'created_at',
			];
		}

		return [
			'id',
			'title',
			'description',
			'body',
			'created_at',
			'image',
			'similar_items',
			'favorite'
		];
	}

	public function getImage() {
		return $this->thumbnail_base_url . '/' . $this->thumbnail_path;
	}

	public function getSimilar_items() {
		self::setSerializeType(self::FIELDS_SIMPLE);
		return self::find()
			->joinWith('similarNews', false)
			->where(['`similar_news`.`news_id`' => $this->id])
			->all();
	}

	public function getFavorite() {
		return (bool)FavoriteNews::find()
			->where([
				'user_id' => \Yii::$app->user->id,
				'news_id' => $this->id
			])
			->count();
	}
}
