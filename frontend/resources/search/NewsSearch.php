<?php

namespace frontend\resources\search;

use frontend\resources\News;

class NewsSearch extends News {
	/**
	 * @var mixed|null
	 */
	public $limit = 12;

	/**
	 * @var mixed|null
	 */
	public $page = 1;

	/**
	 * @var int|null
	 */
	public $offset;

	/**
	 * @var mixed|null
	 */
	public $ascending;

	public $city;
	public $favorite;


	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id'], 'integer'],
			[['title'], 'string'],
			[['limit', 'page', 'order_by', 'ascending', 'offset', 'city', 'favorite'], 'safe'],
		];
	}

	/**
	 * @param $params
	 */
	public function search($params) {
		$this->load($params, '');
		$query = News::find();

		$query
			->joinWith('cities', false)
			->where(['status' => 1, 'city_id' => $this->city]);

		if($this->favorite) {
			$query->joinWith('favoriteNews', false)->andWhere(['user_id' => \Yii::$app->user->id]);
		}

		$query->orderBy('id DESC');

		$query->offset(--$this->page * $this->limit);

		return [
			'count' => $query->count(),
			'data' => $query->limit($this->limit)->all()
		];
	}
}
