<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\YiiAsset;
use yii\web\AssetBundle;
use common\assets\Html5shiv;
use yii\bootstrap4\BootstrapAsset;
use rmrevin\yii\fontawesome\NpmFreeAssetBundle;

/**
 * Frontend application asset
 */
class FrontendAsset extends AssetBundle {
	/**
	 * @var string
	 */
	public $sourcePath = '@frontend/web';
	
	/**
	 * @var array
	 */
	public $css = [
		'css/style.css',
		'css/main-page.css',
		'css/tailwind.css'
	];
	
	/**
	 * @var array
	 */
	public $js = [
		'js/app.js',
		'js/axios.min.js',
		'js/api.js',
		'/js/babel.min.js',
		[
			YII_ENV_DEV ? '/js/react.development.js' : '/js/react-dom.development.js',
			'crossorigin' => true
		],
		['/js/react-dom.development.js', 'crossorigin' => true],
		'node_modules/react-modal/dist/react-modal.min.js',
		'/js/konva.min.js',
		'/js/reactconva.js'
		// '/node_modules/react-konva/lib/invariant.js',
//		'https://cdnjs.cloudflare.com/ajax/libs/konva/7.2.5/konva.min.js',
	];
	
	/**
	 * @var array
	 */
	public $depends = [
		YiiAsset::class,
		BootstrapAsset::class,
		Html5shiv::class,
		NpmFreeAssetBundle::class,
	];
}
