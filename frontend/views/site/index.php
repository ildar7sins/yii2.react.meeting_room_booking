<?php
/**
 * @var yii\web\View $this
 */
$this->title = Yii::$app->name;

use frontend\assets\FrontendAsset;
use common\extensions\AssetsRegister;

AssetsRegister::registerViewAssets([FrontendAsset::class], '@frontendUrl', $this, [], []);
?>
<div id="main-page" class="page-content">
	<div class="container pt-5 pb-5">
		<h1>Hello booking</h1>
	</div>
</div>
