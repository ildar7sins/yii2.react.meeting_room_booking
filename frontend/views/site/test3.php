<?php
/**
 * @var yii\web\View $this
 */
$this->title = Yii::$app->name;

use frontend\assets\FrontendAsset;
use common\extensions\AssetsRegister;

AssetsRegister::registerViewAssets([FrontendAsset::class],
	'@frontendUrl', $this,
	[
		['/js/test3/component.js', 'type' => 'text/babel'],
		['/js/test3/index.js', 'type' => 'text/babel'],
	],
	['/css/main/index.css']
);
?>

<div id="main-page" class="page-content">
    <div class="container pt-5 pb-5">
        <div id="app"></div>
    </div>
</div>
