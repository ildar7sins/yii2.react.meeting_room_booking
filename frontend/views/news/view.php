<?php

/** @var \backend\models\News $model */

$this->title = $model->title;

use frontend\assets\FrontendAsset;
use common\extensions\AssetsRegister;

$this->registerJsVar('news', $model->toArray());
AssetsRegister::registerViewAssets([FrontendAsset::class], '@frontendUrl', $this,
    ['/js/news/view.min.js'], ['/css/news/view.css']);

?>

<div id="news-view" class="page-content position-relative py-4">
    <div class="row">
        <div class="col-12">
            <button class="btn btn-link add-to-favorite" @click="toggleFavorite">
                <template v-if="news.favorite">
                    <i class="fas fa-2x fa-star"></i>
                </template>
                <template v-else>
                    <i class="far fa-2x fa-star"></i>
                </template>
            </button>

            <h1 class="news-title mb-3" class="text-center">{{news.title}}</h1>
            <img class="img-fluid news-image mb-4" :src="news.image" height="200px" alt="News image" width="80%">
            <div class="news-body" v-html="news.body"></div>
        </div>
        <div class="col-12 mt-4">
            <div class="row">
                <div class="col-12 col-md-4" v-for="item in news.similarItems">
                    <news-card :news="item"/>
                </div>
            </div>
        </div>
    </div>
</div>
