<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use yii\helpers\Html;

\frontend\assets\FrontendAsset::register($this);
$this->registerJsVar('API_URL', \yii\helpers\Url::to('@apiUrl/v1'));
$this->registerJsVar('user_access_token', Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->access_token);

$this->registerJsVar('maxPeople', Yii::$app->keyStorage->get('maxPeople'));
$this->registerJsVar('dateStart', Yii::$app->keyStorage->get('dateStart'));
$this->registerJsVar('dateEnd', Yii::$app->keyStorage->get('dateEnd'));
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<?php echo Html::csrfMetaTags() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>
<?php echo $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
