<?php

namespace common\extensions;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;

class AssetsRegister {

	/**
	 * Регистрирует ассетсы на конкретную страницу
	 * @param $depends
	 * @param $view
	 * @param array $js
	 * @param array $css
	 */
	public static function registerViewAssets($depends, $baseUrl, $view, $js = [], $css = []) {
		$asset = new AssetBundle([
			'depends' => is_array($depends) ? $depends : [$depends],
			'js' => $js,
			'css' => $css,
			'basePath' => '@webroot',
			'baseUrl' => Url::to($baseUrl)
		]);

		$path = Yii::$app->requestedRoute;
		$view->getAssetManager()->bundles["agentCrm/{$path}"] = $asset;
		$view->registerAssetBundle("agentCrm/{$path}");
	}
}
