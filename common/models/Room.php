<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "room".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property int|null $people
 * @property float|null $price
 * @property int|null $created_by
 * @property int|null $created_at
 * @property int|null $updated_by
 * @property int|null $updated_at
 * @property int $status
 * @property string|null $plan
 *
 * @property User $createdBy
 * @property RoomAttachment[] $roomAttachments
 * @property RoomService[] $roomServices
 * @property AdditionalService[] $services
 * @property User $updatedBy
 */
class Room extends \yii\db\ActiveRecord {
	
	const STATUS_NOT_ACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
	/**
	 * @var
	 */
	public $attachments;
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'room';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['people', 'status'], 'integer'],
			[['status'], 'integer'],
			['status', 'default', 'value' => 1],
			[['price'], 'number'],
			[['plan'], 'safe'],
			[['title', 'description'], 'string', 'max' => 255],
			[['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
			[['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
			[['attachments', 'services'], 'safe'],
		];
	}
	
	/**
	 * @return array|\yii\db\ActiveRecord[]
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getServices() {
		return $this->getAdditionalServices()->all();
	}
	
	/**
	 * @param $value
	 */
	public function setServices($value) {
		RoomService::deleteAll(['room_id' => $this->id]);
		if ($value) {
			foreach ($value as $service) {
				$city = new RoomService();
				$city->room_id = $this->id;
				$city->service_id = (int)$service;
				$city->save();
			}
		}
	}
	
	
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::class,
			BlameableBehavior::class,
			[
				'class' => UploadBehavior::class,
				'attribute' => 'attachments',
				'multiple' => true,
				'uploadRelation' => 'roomAttachments',
				'pathAttribute' => 'path',
				'baseUrlAttribute' => 'base_url',
				'orderAttribute' => 'order',
				'typeAttribute' => 'type',
				'sizeAttribute' => 'size',
				'nameAttribute' => 'name',
			],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'title' => 'Название',
			'description' => 'Описание',
			'people' => 'Макс. вместимость',
			'price' => 'Цена за час',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'status' => 'Работает',
			'plan' => 'План комнаты',
			'services' => 'Дополнительные услуги'
		];
	}
	
	/**
	 * Gets query for [[CreatedBy]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatedBy() {
		return $this->hasOne(User::className(), ['id' => 'created_by']);
	}
	
	/**
	 * Gets query for [[RoomAttachments]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getRoomAttachments() {
		return $this->hasMany(RoomAttachment::className(), ['room_id' => 'id']);
	}
	
	/**
	 * Gets query for [[RoomServices]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getRoomServices() {
		return $this->hasMany(RoomService::className(), ['room_id' => 'id']);
	}
	
	/**
	 * Gets query for [[Services]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getAdditionalServices() {
		return $this->hasMany(AdditionalService::className(), ['id' => 'service_id'])->viaTable('room_service', ['room_id' => 'id']);
	}
	
	/**
	 * Gets query for [[UpdatedBy]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUpdatedBy() {
		return $this->hasOne(User::className(), ['id' => 'updated_by']);
	}
}
