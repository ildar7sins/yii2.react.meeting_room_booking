<?php

namespace common\models\search;

use yii\base\Model;
use common\models\Room;
use yii\data\ActiveDataProvider;

/**
 * RoomSearch represents the model behind the search form about `common\models\Room`.
 */
class RoomSearch extends Room {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'people', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'], 'integer'],
			[['title', 'description', 'plan'], 'safe'],
			[['price'], 'number'],
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Room::find();
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere([
			'id' => $this->id,
			'people' => $this->people,
			'price' => $this->price,
			'created_by' => $this->created_by,
			'created_at' => $this->created_at,
			'updated_by' => $this->updated_by,
			'updated_at' => $this->updated_at,
			'status' => $this->status,
		]);
		
		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'plan', $this->plan]);
		
		return $dataProvider;
	}
}
