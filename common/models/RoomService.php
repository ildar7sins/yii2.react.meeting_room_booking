<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "room_service".
 *
 * @property int $room_id
 * @property int $service_id
 *
 * @property Room $room
 * @property AdditionalService $service
 */
class RoomService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'room_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'service_id'], 'required'],
            [['room_id', 'service_id'], 'integer'],
            [['room_id', 'service_id'], 'unique', 'targetAttribute' => ['room_id', 'service_id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalService::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'service_id' => 'Service ID',
        ];
    }

    /**
     * Gets query for [[Room]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * Gets query for [[Service]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(AdditionalService::className(), ['id' => 'service_id']);
    }
}
