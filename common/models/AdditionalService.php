<?php

namespace common\models;

/**
 * This is the model class for table "additional_service".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property float|null $price
 * @property int $status
 *
 * @property RoomService[] $roomServices
 * @property Room[] $rooms
 */
class AdditionalService extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'additional_service';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['price'], 'number'],
			[['status'], 'integer'],
			[['title', 'description'], 'string', 'max' => 255],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'title' => 'Название',
			'description' => 'Описание',
			'price' => 'Цена',
			'status' => 'Статус',
		];
	}
	
	/**
	 * Gets query for [[RoomServices]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getRoomServices() {
		return $this->hasMany(RoomService::className(), ['service_id' => 'id']);
	}
	
	/**
	 * Gets query for [[Rooms]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getRooms() {
		return $this->hasMany(Room::className(), ['id' => 'room_id'])->viaTable('room_service', ['service_id' => 'id']);
	}
}
