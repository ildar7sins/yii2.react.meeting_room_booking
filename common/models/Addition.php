<?php

namespace common\models;

/**
 * This is the model class for table "addition".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property float|null $price
 * @property int $status
 *
 * @property BookingAddition[] $bookingAdditions
 */
class Addition extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'addition';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['price'], 'number'],
			[['status'], 'integer'],
			[['title', 'description'], 'string', 'max' => 255],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'price' => 'Price',
			'status' => 'Status',
		];
	}
	
	/**
	 * Gets query for [[BookingAdditions]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getBookingAdditions() {
		return $this->hasMany(BookingAddition::className(), ['addition_id' => 'id']);
	}
}
