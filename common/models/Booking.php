<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int $room_id
 * @property int $date_start
 * @property int $date_end
 * @property int $status
 * @property int|null $created_by
 * @property int $created_at
 * @property int|null $updated_by
 * @property int $updated_at
 * @property int $customer_id
 * @property string $comment
 * @property float|null $price
 */
class Booking extends \yii\db\ActiveRecord {
	const STATUS_NEW = 1;
	const STATUS_ACTIVE = 2;
	const STATUS_CANCELED = 3;
	const STATUS_DELETED = 4;
	const STATUS_FINISHED = 5;
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'booking';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['room_id', 'date_start', 'date_end', 'created_at', 'updated_at', 'customer_id'], 'required'],
			[['room_id', 'date_start', 'date_end', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at', 'customer_id'], 'integer'],
			[['price'], 'number'],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'room_id' => 'Переговорная',
			'date_start' => 'Дата бронирования',
			'date_end' => 'Кол-во часов',
			'status' => 'Статус',
			'created_by' => 'Created By',
			'created_at' => 'Создан',
			'updated_by' => 'Updated By',
			'updated_at' => 'Updated At',
			'customer_id' => 'Пользователь',
			'price' => 'Цена',
			'comment' => 'Комментарий',
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::class,
			BlameableBehavior::class,
		];
	}
	
	/**
	 * Returns user statuses list
	 * @return array|mixed
	 */
	public static function statuses() {
		return [
			self::STATUS_NEW => 'Новое',
			self::STATUS_ACTIVE => 'Активное',
			self::STATUS_FINISHED => 'Завершёно',
			self::STATUS_CANCELED => 'Отменён',
			self::STATUS_DELETED => 'Удалён',
		];
	}
}
