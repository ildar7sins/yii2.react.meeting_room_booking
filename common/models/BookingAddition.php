<?php

namespace common\models;

/**
 * This is the model class for table "booking_addition".
 *
 * @property int $booking_id
 * @property int $addition_id
 * @property int $count
 *
 * @property Addition $addition
 */
class BookingAddition extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'booking_addition';
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['booking_id', 'addition_id'], 'required'],
			[['booking_id', 'addition_id'], 'integer'],
			[['booking_id', 'addition_id'], 'unique', 'targetAttribute' => ['booking_id', 'addition_id']],
			[['addition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addition::className(), 'targetAttribute' => ['addition_id' => 'id']],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'booking_id' => 'Booking ID',
			'addition_id' => 'Addition ID',
		];
	}
	
	/**
	 * Gets query for [[Addition]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getAddition() {
		return $this->hasOne(Addition::className(), ['id' => 'addition_id']);
	}
}
