<?php

namespace common\actions;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\ServerErrorHttpException;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class CreateAction extends Action {
	/**
	 * @var string the scenario to be assigned to the new model before it is validated and saved.
	 */
	public $scenario = Model::SCENARIO_DEFAULT;
	
	/**
	 * @var string the name of the view action. This property is needed to create the URL when the model is successfully created.
	 */
	public $viewAction = 'view';
	
	
	/**
	 * Creates a new model.
	 * @return \yii\db\ActiveRecordInterface the model newly created
	 * @throws ServerErrorHttpException if there is any error when creating the model
	 */
	public function run() {
		$data = Yii::$app->query->getData();
		
		if ($this->checkAccess) {
			call_user_func($this->checkAccess, $this->id);
		}
		
		/* @var $model \yii\db\ActiveRecord */
		$model = new $this->modelClass([
			'scenario' => $this->scenario,
		]);
		
		$model->load($data, '');
		if ($model->save()) {
			$response = Yii::$app->getResponse();
			$response->setStatusCode(201);
			$id = implode(',', array_values($model->getPrimaryKey(true)));
			$response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
		} elseif (!$model->hasErrors()) {
			throw new ServerErrorHttpException('Не удалось сохранить данные');
		}
		
		return $model;
	}
}
