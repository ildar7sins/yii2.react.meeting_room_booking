<?php

namespace common\actions;

use yii\rest\Action;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class FindAction extends Action {
	
	/**
	 * Массив полей для поиска...
	 * @var
	 */
	public $searchFields;
	
	/**
	 * @var string
	 */
	public $singleLabel = 'Модель';
	
	/**
	 * Displays a model.
	 * @param string $id the primary key of the model.
	 * @return \yii\db\ActiveRecordInterface the model being displayed
	 */
	public function run($q) {
		$checkDelete = new $this->modelClass();
		
		$query = $this->modelClass::find();
		
		foreach ($this->searchFields as $field) {
			$query->filterWhere(['LIKE', $this->modelClass::col($field), $q]);
		}
		
		if ($checkDelete->hasAttribute('deleted')) {
			$query->andWhere(['deleted' => 0]);
		}
		
		return $query->all();
	}
}
