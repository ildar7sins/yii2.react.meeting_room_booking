<?php

namespace common\actions;

use yii\rest\Action;
use yii\web\NotFoundHttpException;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class ViewAction extends Action {
	
	/**
	 * @var string
	 */
	public $singleLabel = 'Модель';
	
	/**
	 * Displays a model.
	 * @param string $id the primary key of the model.
	 * @return \yii\db\ActiveRecordInterface the model being displayed
	 */
	public function run($id) {
		$model = $this->findModel($id);
		if ($this->checkAccess) {
			call_user_func($this->checkAccess, $this->id, $model);
		}
		
		return $model;
	}
	
	/**
	 * Finds the Specialization model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return object
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function findModel($id) {
		$checkDelete = new $this->modelClass();
		$params = $checkDelete->hasAttribute('deleted') ? ['id' => $id, 'deleted' => 0] : $id;
		
		if (($model = $this->modelClass::findOne($params)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException("{$this->singleLabel} не найдена.");
		}
	}
}
