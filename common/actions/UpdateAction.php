<?php

namespace common\actions;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\rest\Action;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class UpdateAction extends Action {
	/**
	 * @var string the scenario to be assigned to the new model before it is validated and saved.
	 */
	public $scenario = Model::SCENARIO_DEFAULT;
	
	/**
	 * @var string
	 */
	public $singleLabel = 'Модель';
	
	/**
	 * @var string the name of the view action. This property is needed to create the URL when the model is successfully created.
	 */
	public $viewAction = 'view';
	
	
	/**
	 * Creates a new model.
	 * @return \yii\db\ActiveRecordInterface the model newly created
	 * @throws ServerErrorHttpException if there is any error when creating the model
	 */
	public function run($id) {
		$data = Yii::$app->query->getData();
		
		/* @var $model ActiveRecord */
		$model = $this->findModel($id);
		
		if ($this->checkAccess) {
			call_user_func($this->checkAccess, $this->id, $model);
		}
		
		$model->scenario = $this->scenario;
		$model->load($data, '');
		
		if ($model->save() === false && !$model->hasErrors()) {
			throw new ServerErrorHttpException('Не удалось сохранить изменения, попробуйте позже');
		}
		
		return $model;
	}
	
	/**
	 * Finds the Specialization model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return object
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function findModel($id) {
		$checkDelete = new $this->modelClass();
		$params = $checkDelete->hasAttribute('deleted') ? ['id' => $id, 'deleted' => 0] : $id;
		
		if (($model = $this->modelClass::findOne($params)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException("{$this->singleLabel} не найдена.");
		}
	}
}
