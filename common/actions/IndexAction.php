<?php

namespace common\actions;

use Yii;
use yii\rest\Action;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class IndexAction extends Action {
	public $searchModel;
	
	/**
	 * Displays a model.
	 * @param string $id the primary key of the model.
	 * @return \yii\db\ActiveRecordInterface the model being displayed
	 */
	public function run() {
		$userSearch = new $this->searchModel();
		return $userSearch->search(Yii::$app->request->get());
	}
}
