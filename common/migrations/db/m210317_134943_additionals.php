<?php

use yii\db\Migration;

/**
 * Class m210317_134943_additionals
 */
class m210317_134943_additionals extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('addition', [
			'id' => $this->primaryKey(),
			'title' => $this->string(255),
			'description' => $this->string(),
			'price' => $this->float(),
			'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
		]);
		
		$this->createIndex('idx-addition-title', 'addition', 'title');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropIndex('idx-addition-title', 'addition');
		
		$this->dropTable('addition');
	}
}
