<?php

use yii\db\Migration;

/**
 * Class m210316_131211_update_booking
 */
class m210316_131211_update_booking extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('booking', 'comment', $this->string()->after('date_end'));
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn('booking', 'comment');
	}
}
