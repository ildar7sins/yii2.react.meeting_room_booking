<?php

use yii\db\Migration;

/**
 * Class m210315_154955_room_service
 */
class m210315_154955_room_service extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('room_service', [
			'room_id' => $this->integer()->notNull(),
			'service_id' => $this->integer()->notNull(),
		]);
		
		$this->addPrimaryKey('room_service_pk', 'room_service', ['room_id', 'service_id']);
		$this->addForeignKey(
			'fk-room_service-room_id', 'room_service',
			'room_id', 'room', 'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-room_service-service_id', 'room_service',
			'service_id', 'additional_service', 'id',
			'CASCADE',
			'CASCADE'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-room_service-room_id', 'room_service');
		$this->dropForeignKey('fk-room_service-service_id', 'room_service');
		$this->dropTable('room_service');
	}
}
