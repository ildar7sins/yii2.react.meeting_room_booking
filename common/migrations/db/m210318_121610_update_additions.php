<?php

use yii\db\Migration;

/**
 * Class m210318_121610_update_additions
 */
class m210318_121610_update_additions extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('booking_addition', 'count', $this->integer()->notNull()->defaultValue(1));
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn('booking_addition', 'count');
	}
}
