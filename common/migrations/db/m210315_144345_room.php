<?php

use yii\db\Migration;

/**
 * Class m210315_144345_room
 */
class m210315_144345_room extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('room', [
			'id' => $this->primaryKey(),
			'title' => $this->string(),
			'description' => $this->string(),
			'people' => $this->smallInteger(),
			'price' => $this->float(),
			'created_by' => $this->integer(),
			'created_at' => $this->integer(),
			'updated_by' => $this->integer(),
			'updated_at' => $this->integer(),
			'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
			'plan' => $this->json(),
		]);
		
		$this->addForeignKey('fk-room-created_by', 'room',
			'created_by', 'user', 'id',
			'SET NULL', 'CASCADE'
		);
		$this->addForeignKey('fk-room-updated_by', 'room',
			'updated_by', 'user', 'id',
			'SET NULL', 'CASCADE'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-room-created_by', 'room');
		$this->dropForeignKey('fk-room-updated_by', 'room');
		
		$this->dropTable('room');
	}
}
