<?php

use yii\db\Migration;

/**
 * Class m210317_070225_update
 */
class m210317_070225_update extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('room_attachment', 'order', $this->integer());
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn('room_attachment', 'order');
	}
	
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m210317_070225_update cannot be reverted.\n";

		return false;
	}
	*/
}
