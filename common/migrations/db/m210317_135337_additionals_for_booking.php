<?php

use yii\db\Migration;

/**
 * Class m210317_135337_additionals_for_booking
 */
class m210317_135337_additionals_for_booking extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('booking_addition', [
			'booking_id' => $this->integer()->notNull(),
			'addition_id' => $this->integer()->notNull(),
		]);
		
		$this->addPrimaryKey('booking_addition_pk', 'booking_addition', ['booking_id', 'addition_id']);
		
		$this->addForeignKey(
			'fk-booking_addition-addition_id', 'booking_addition',
			'addition_id', 'addition', 'id',
			'CASCADE',
			'CASCADE'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-booking_addition-addition_id', 'booking_addition');
		$this->dropTable('booking_addition');
	}
}
