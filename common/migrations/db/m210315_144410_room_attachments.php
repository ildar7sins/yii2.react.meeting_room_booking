<?php

use yii\db\Migration;

/**
 * Class m210315_144410_room_attachments
 */
class m210315_144410_room_attachments extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('room_attachment', [
			'room_id' => $this->integer()->notNull(),
			'path' => $this->string()->notNull(),
			'base_url' => $this->string(),
			'type' => $this->string(),
			'size' => $this->integer(),
			'name' => $this->string(),
			'created_at' => $this->integer(),
		]);
		$this->addPrimaryKey('room_attachment_pk', 'room_attachment', ['room_id', 'path', 'base_url']);
		$this->addForeignKey(
			'fk-room_attachment-room_id', 'room_attachment',
			'room_id', 'room', 'id',
			'CASCADE',
			'CASCADE'
		);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-room_attachment-room_id', 'room_attachment');
		$this->dropTable('room_attachment');
	}
}
