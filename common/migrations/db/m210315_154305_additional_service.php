<?php

use yii\db\Migration;

/**
 * Class m210315_154305_additional_service
 */
class m210315_154305_additional_service extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('additional_service', [
			'id' => $this->primaryKey(),
			'title' => $this->string(255),
			'description' => $this->string(),
			'price' => $this->float(),
			'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
		]);
		
		$this->createIndex('idx-additional_service-title', 'additional_service', 'title');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropIndex('idx-additional_service-title', 'additional_service');
		
		$this->dropTable('additional_service');
	}
}
