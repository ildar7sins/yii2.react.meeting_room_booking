<?php

use yii\db\Migration;

/**
 * Class m210315_155921_booking
 */
class m210315_155921_booking extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('booking', [
			'id' => 'INT NOT NULL AUTO_INCREMENT',
			'room_id' => 'INT NOT NULL',
			'date_start' => $this->integer()->notNull(),
			'date_end' => $this->integer()->notNull(),
			'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
			'created_by' => $this->integer(),
			'created_at' => $this->integer()->notNull(),
			'updated_by' => $this->integer(),
			'updated_at' => $this->integer()->notNull(),
			'customer_id' => $this->integer()->notNull(),
			'price' => $this->float(),
			'PRIMARY KEY (room_id,id)'
		], 'ENGINE=MyISAM');
		
		$this->addForeignKey('fk-booking-customer_id', 'booking',
			'customer_id', 'user', 'id',
			'CASCADE', 'CASCADE'
		);
		$this->createIndex('idx-booking-customer_id', 'booking', 'customer_id');
		$this->createIndex('idx-booking-dates', 'booking', ['date_start', 'date_end']);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-booking-customer_id', 'booking');
		$this->dropIndex('idx-booking-customer_id', 'booking');
		$this->dropIndex('idx-booking-dates', 'booking');
		
		$this->dropTable('booking');
	}
}
